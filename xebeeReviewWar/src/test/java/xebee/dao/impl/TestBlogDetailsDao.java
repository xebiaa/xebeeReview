package xebee.dao.impl;

import static org.junit.Assert.assertEquals;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import xebee.dao.BlogDetailsDao;

@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-config.xml")
public class TestBlogDetailsDao {

	@Resource
	private BlogDetailsDao blogDetailsDao;
	
	@Test
	public void shouldGetAllBlogs() {
		
		assertEquals(1, blogDetailsDao.getAllBlogs().size());
	}
}
