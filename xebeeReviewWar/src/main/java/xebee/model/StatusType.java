package xebee.model;

public enum StatusType {

	TO_BE_ASSIGNED,
	IN_PROGRESS,
	APPROVED,
	REJECTED
}
