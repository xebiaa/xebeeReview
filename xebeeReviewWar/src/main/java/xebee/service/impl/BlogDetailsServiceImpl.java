package xebee.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Service;

import xebee.dao.BlogDetailsDao;
import xebee.model.BlogDetails;
import xebee.service.BlogDetailsService;

@Service("blogDetailsService")
@RemotingDestination(channels="my-amf")
public class BlogDetailsServiceImpl implements BlogDetailsService {

	@Autowired
	BlogDetailsDao blogDetailsDao;
	
	@Override
	@RemotingInclude
	public List<BlogDetails> getAllBlogs() {
		
		return blogDetailsDao.getAllBlogs();
	}

}
