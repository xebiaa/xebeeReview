package xebee.service;

import java.util.List;
import xebee.model.BlogDetails;


public interface BlogDetailsService {

	public List<BlogDetails> getAllBlogs();
	
	
}
