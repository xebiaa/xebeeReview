package xebee.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import xebee.dao.BlogDetailsDao;
import xebee.model.BlogDetails;

@Repository("blogDetailsDao")
public class BlogDetailsDaoImpl implements BlogDetailsDao {

	protected HibernateTemplate hibernateTemplate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BlogDetails> getAllBlogs() {
		return hibernateTemplate.findByExample(new BlogDetails());
	}

}
