package xebee.dao;

import java.util.List;

import xebee.model.BlogDetails;

public interface BlogDetailsDao {

	public List<BlogDetails> getAllBlogs();
}
