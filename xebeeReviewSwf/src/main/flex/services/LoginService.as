package services
{
	import mx.rpc.AsyncToken;
	
	import utils.ChannelSetBean;

	public class LoginService
	{
		[Bindable]
		public var channelSetBean:ChannelSetBean;
		
		public function login(username:String,password:String):AsyncToken{
			return channelSetBean.channelSet.login(username,password);
		}
	}
}