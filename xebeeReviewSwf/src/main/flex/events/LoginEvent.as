package events
{
	import flash.events.Event;
	
	public class LoginEvent extends Event
	{
		public static const LOGIN:String="Login";
		
		private var _username:String;
		
		private var _password:String;
		
		public function LoginEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public function get username():String{
			return _username;
		}
		
		public function set username(value:String):void{
			_username = value;
		}
		
		public function get password():String{
			return _password;
		}
		
		public function set password(value:String):void{
			_password = value;
		}
	}
}