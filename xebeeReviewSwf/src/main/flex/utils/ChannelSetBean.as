package utils
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;

	public class ChannelSetBean extends EventDispatcher
	{
		private var _channelSet:ChannelSet;
		private var _dispatcher:IEventDispatcher;
		
		public function ChannelSetBean(dispatcher:IEventDispatcher)
		{
			_dispatcher = dispatcher;
			setChannelSet();
		}
		
		[Bindable(Event="channelSetCreated")]
		public function get channelSet():ChannelSet{
			return _channelSet;
		}
		
		private function setChannelSet():void{
			var amfChannel:AMFChannel = new AMFChannel("my-amf","http://localhost:8080/messagebroker/amf");
			amfChannel.pollingEnabled = false;
			_channelSet = new ChannelSet();
			_channelSet.addChannel(amfChannel);
			_dispatcher.dispatchEvent(new Event("channelSetCreated"));
		}
	}
}