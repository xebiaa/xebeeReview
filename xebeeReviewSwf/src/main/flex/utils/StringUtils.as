package utils
{
	import mx.utils.StringUtil;

	public class StringUtils
	{
		public static function isBlank(text:String):Boolean{
			return text == null || StringUtil.trim(text).length == 0;
		}
	}
}